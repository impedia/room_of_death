﻿using UnityEngine;
using System.Collections;

[RequireComponent (typeof(AudioSource))]
public class ChickenController : MonoBehaviour {

	int rando_int;
	public Collider2D col;
	int i;
	public AudioClip playsound;	//The sound
	AudioSource audio;	//The Sound Player

	// Use this for initialization
	void Start () {
		audio = GetComponent<AudioSource> (); 
		rando_int = Random.Range(10,60);
		Debug.Log(rando_int);
	}
	
	// Update is called once per frame
	void Update () {

		StartCoroutine(Spawn());

	}

	IEnumerator Spawn(){
		if(i == 0)
		{
		i = 1;
		yield return new WaitForSeconds(rando_int);
		col.enabled = true;
		this.gameObject.GetComponent<Rigidbody2D>().AddForce(new Vector2(1f,0f));
		yield return new WaitForSeconds (2f);
		audio.PlayOneShot(playsound, 1F);
		}

	}



}
