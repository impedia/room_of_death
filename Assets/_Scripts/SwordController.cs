﻿using UnityEngine;
using System.Collections;

[RequireComponent (typeof(AudioSource))]
public class SwordController : MonoBehaviour {

	public AudioClip playsound;	//The sound
	AudioSource audio;	//The Sound Player

	public GameObject bulletPre_GO;
	
	public Transform startPos_tr;
	
	private Rigidbody2D rb;
	
	public GameObject cry;
	
	public GameObject head;
	
	private Transform mousePos_vct3;
	
	private Vector3 angle;

	public GameObject tear;
	
	int i;
	
	
	// Use this for initialization
	void Start () {
		audio = GetComponent<AudioSource> (); 
		rb = bulletPre_GO.GetComponent<Rigidbody2D>();
		i = 0;
	}
	
	// Update is called once per frame
	void Update () {
		
		if(Input.GetKey(KeyCode.Mouse0))
		{
			cry.SetActive(true);
			
		} else {
			cry.SetActive(false);
			
		}
		
		var pos = Camera.main.WorldToScreenPoint(transform.position);
		var dir = Input.mousePosition - pos;
		var angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
		head.transform.rotation = Quaternion.AngleAxis(angle -90f, Vector3.forward);
		
		if (Input.GetKeyDown(KeyCode.Mouse0))
		{
			//Vector3 pos = Input.mousePosition;
			//pos.z = transform.position.z - Camera.main.transform.position.z;
			//pos = Camera.main.ScreenToWorldPoint(pos);
			
			//Quaternion q = Quaternion.FromToRotation(Vector3.up, pos - transform.position);
			//GameObject go = Instantiate(bulletPre_GO, startPos_tr.transform.position, Quaternion.identity) as GameObject;
			//go.GetComponent<Rigidbody2D>().AddForce(go.transform.up * 500.0);
			//			mousePos_vct3 = Input.mousePosition;
			//			mousePos_vct3 = Camera.main.ScreenToWorldPoint(mousePos_vct3);
			StartCoroutine(makeBullet());
			
			//			GameObject rb = Instantiate(bulletPre_GO, startPos_tr.transform.position, startPos_tr.transform.rotation) as GameObject;
			//			rb.GetComponent<Rigidbody2D>().AddRelativeForce(new Vector2(0, 1 *500f) , ForceMode2D.Force);
			//			Destroy(rb,1);
			// rb = bulletPre_GO.GetComponent<Rigidbody2D>();
			//rb.AddForce(Input.mousePosition * 50);
			
		}
		//rb.AddForce(Input.mousePosition * 50);
		
	}
	
	IEnumerator makeBullet()
	{
		if(i == 0)
		{
			i = 1;
			tear.SetActive(false);
			GameObject rb = Instantiate(bulletPre_GO, startPos_tr.transform.position, startPos_tr.transform.rotation) as GameObject;
			rb.GetComponent<Rigidbody2D>().AddRelativeForce(new Vector2(0, 1 *500f) , ForceMode2D.Force);
			audio.PlayOneShot(playsound, 1F);
			Destroy(rb,1);
			yield return new WaitForSeconds(1f);
			tear.SetActive(true);
			i = 0;
		}
	}
	
	
	
	
	
	
	
}
