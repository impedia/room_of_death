﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour {

	public int points;
	public Text pointsText;

	public GameObject head_GO;

	public bool isDead_BO;
	
	public GameObject DeadMenu_Canvas_GO;

	private Transform mousePos_tf;

	public  GameObject arms;

	private Vector3 angle_vct3;
	public float speed = 5f;//players movement speed
	public float moveForce = 365f;
	private float movex = 0f;//variables to move the player
	private float movey = 0f;//variables to move the player
	private float xaxis;
	private float yaxis;
	private int i = 0;
	public float jumpheight = 1000;
	private float xaxisOld;
	private float yaxisOld;
	public Rigidbody2D rb;
	bool moving;
	private bool isgrounded = false;
	public Text chickren;
	private int chickenint = 0;
	public bool shicken = false;
//	public GameObject death;
	public float chickenTime = 10;

	public Animator anim;
	int e = 0;

	
	// Use this for initialization
	void Start () {
		points = 0;
//		death.SetActive(false);
		isDead_BO = false;
		rb = this.GetComponent<Rigidbody2D>();
	}
	
	// Update is called once per frame
	void Update () {

		pointsText.text = "Points: " + points;
		if(shicken == true)
		{
			if(chickenTime >= 0f)
			{
			//StartCoroutine(chickentimer());
			chickenTime -= Time.deltaTime;
			int tmp = System.Convert.ToInt32(chickenTime);
			chickren.text = tmp.ToString();
			} else {
				chickren.text = "";
			}

		}else {
			chickren.text = "" ;
		}


		xaxis = transform.position.x;
		if (isDead_BO) {
			DeadMenu_Canvas_GO.SetActive (true);
			//Time.timeScale = 0f;
			//sword_GO.SetActive(false);
		} else {
			DeadMenu_Canvas_GO.SetActive(false);
			Time.timeScale = 1f;
		}
//		mousePos_tf.position =  Camera.main.ScreenToWorldPoint(Input.mousePosition);
//		angle_vct3 = mousePos_tf.position;
		var pos = Camera.main.WorldToScreenPoint(transform.position);
		var dir = Input.mousePosition - pos;
		var angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
		if(angle >= 90 - 90){
			//Debug.Log(angle);
		
			if(angle <= 90)
			{
				head_GO.transform.rotation = Quaternion.AngleAxis(-angle +45f, Vector3.forward);
				this.gameObject.transform.localScale = new Vector3(-0.5f, 0.5f, 1f);
			} else {
				head_GO.transform.rotation = Quaternion.AngleAxis(angle -135f, Vector3.forward);
				this.gameObject.transform.localScale = new Vector3(0.5f, 0.5f, 1f);
			}
		
		}

		if(Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.RightArrow) || Input.GetKey(KeyCode.LeftArrow))
		{
			anim.SetBool("Run", true);
		} else {
			anim.SetBool("Run", false);
		}

	
		//StartCoroutine(Move());
	}

	public void AddScore (int newPointsValue)
	{
		points += newPointsValue;

	}
	


	void FixedUpdate () {
		if (i == 0) {
			movex = Input.GetAxis ("Horizontal");//getting the horizontal axis for the movement
			//			GetComponent<Rigidbody2D> ().velocity = new Vector2 (movex * speed, movey * speed);//adding force to the rigidbody in the direction inputed
			//			StartCoroutine(movePlayer());//start the function movePlayer
			if(transform.position.x >= 8){
				transform.position = new Vector2(7.99f, transform.position.y);
			}

			if(transform.position.x <= -8){
				transform.position = new Vector2(-7.99f, transform.position.y);
			}

			if (movex * rb.velocity.x < speed)
				rb.AddForce(Vector2.right * movex * moveForce);
			
			if (Mathf.Abs (rb.velocity.x) > speed)
				rb.velocity = new Vector2(Mathf.Sign (rb.velocity.x) * speed, rb.velocity.y);

		}

	}
//	public void Reset()
//	{
//		
//		isDead_BO = true;
//		death.SetActive(true);
//		death.gameObject.transform.parent = null;
//
//
//	}
//	public void ResetGame(){
//		Application.LoadLevel (Application.loadedLevel);//restart the level
//		
//	}
//	public void MainMenu(){
//	Application.LoadLevel ("MainMenu");//restart the level
//		
//	}

//	IEnumerator chickentimer(){
//
////		for (int i = 1; i<10; i++)
////		{
//			yield return new WaitForSeconds(1f);
//
//			chickenint = 10 -i;
//			chickren.text = "" + chickenint;
//
////		}
//
//	}
//




void OnCollisionStay2D(Collision2D col){
	if(col.collider.CompareTag("Ground")){
//		jump();
	}
		
}

	void OnCollisionEnter2D(Collision2D col){
		if(col.collider.CompareTag("Chicken")){
			shicken = true;
			Destroy(col.gameObject);
			arms.GetComponent<SlashController>().Chick();
		}

	}

//void jump(){
//	if( Input.GetKeyDown (KeyCode.Space)){//getting the vertical axis for the movement
//		//			GetComponent<Rigidbody2D> ().AddForce (transform.up * jumpheight);
//		rb.AddForce(new Vector2(0f, jumpheight));
//		Debug.Log("jump!");
//	}
//}

}



//	IEnumerator Move(){
//
//		if (e ==0){
//			e = 1;
//			xaxisOld = xaxis;
//			yield return new WaitForSeconds(0.05f);
//			if(xaxisOld == xaxis)
//			{
//				anim.SetBool("Run", false);
//				
//			} else {
//				anim.SetBool("Run", true);
//				
//			}
//
//		}
//
//	}
//
//
//






