﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[RequireComponent (typeof(AudioSource))]
public class SlashController : MonoBehaviour {

	public AudioClip playsound;	//The sound
	AudioSource audio;	//The Sound Player

	public GameObject Sword_GO;
	public bool Slashing_BO;
	public Animator changeSide;
	public Vector3 mousePosition;

	public GameObject chicken_GO;

	public Collider2D col;

	bool chickenbool = false;

	int i = 0;
	// Use this for initialization
	void Start () {
		audio = GetComponent<AudioSource> (); 
		changeSide = this.gameObject.GetComponent<Animator>();
		//Sword_GO.SetActive(false);
		Slashing_BO = true;
		col = Sword_GO.GetComponent<Collider2D>();
		col.enabled = false;
	}
	
	// Update is called once per frame
	void Update () {

		mousePosition = Input.mousePosition;
		mousePosition = Camera.main.WorldToScreenPoint(mousePosition);
		StartCoroutine(ChangeSide());
		var pos = Camera.main.WorldToScreenPoint(transform.position);
		var dir = Input.mousePosition - pos;
		var angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;

		if (Slashing_BO == true)
		{
			if (Input.GetButtonDown("Fire1"))
			{
				if(angle >= 90 - 90)
				{
//					StartCoroutine(Slash());
				}
			}
		}
	}

	public void Chick(){

		StartCoroutine(Chicken());
	}


	public IEnumerator Chicken(){

		chickenbool = true;
		Sword_GO.SetActive(false);
		chicken_GO.SetActive(true);
		changeSide.SetBool ("Slash", true);
		col = chicken_GO.GetComponent<Collider2D>();
		yield return new WaitForSeconds(10f);
		Sword_GO.SetActive(true);
		col = Sword_GO.GetComponent<Collider2D>();
		chicken_GO.SetActive(false);
		changeSide.SetBool ("Slash", false);
		chickenbool = false;

	}

//	IEnumerator Slash(){
//		Sword_GO.SetActive(true);
//		Slashing_BO = false;
//		yield return new WaitForSeconds(0.50f);
//		Sword_GO.SetActive(false);
//		yield return new WaitForSeconds(0.50f);
//		Slashing_BO = true;
//	}


	IEnumerator ChangeSide(){

		if (Input.GetKeyDown(KeyCode.Mouse1))
		{
			if(chickenbool == false){
				if ( i == 0) 
				{
					i = 1;
					audio.PlayOneShot(playsound, 1F);
					col.enabled = true;
					changeSide.SetBool ("Slash", true);

					yield return new WaitForSeconds (0.33f);
					changeSide.SetBool ("Slash", false);
					col.enabled = false;
					i = 0;
				}
			}
		}
	}
}
