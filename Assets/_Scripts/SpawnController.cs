﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SpawnController : MonoBehaviour {

	public List <Transform> spawnPoints_list;

	public List <GameObject> enemy_pref;

	private int randoEnemy_int;

	private int rando_int;

	public float time_flt;

	public static float speed_ft;

	int i = 0;

	// Use this for initialization
	void Start () {
		time_flt = 3f;
		speed_ft = 1;
	}

	// Update is called once per frame
	void Update () {
		randoEnemy_int = Random.Range(0, 4);
		rando_int = Random.Range(0,7);
		if(i == 0){
			StartCoroutine(Spawn());
		}

	}

	IEnumerator Spawn(){
		i = 1;
		Instantiate(enemy_pref[randoEnemy_int], spawnPoints_list[rando_int].position, Quaternion.identity);
		//rig.transform.SetParent(GameObject.FindWithTag("EnemyGroup").transform);
		yield return new WaitForSeconds(time_flt);
		i = 0;
		if(time_flt >= 0.7f)
		time_flt -= 0.1f;
		speed_ft += 0.2f;

	}















}
