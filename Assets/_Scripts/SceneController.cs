﻿using UnityEngine;
using System.Collections;

public class SceneController : MonoBehaviour {
	public bool isDead_BO;
	
	public GameObject DeadMenu_Canvas_GO;

	public GameObject death;

    public string level = ""; 

	public Animator sleep;


	void Update(){
		if (isDead_BO) {
			DeadMenu_Canvas_GO.SetActive (true);
			//Time.timeScale = 0f;
			//sword_GO.SetActive(false);
		} else {
			DeadMenu_Canvas_GO.SetActive(false);
			Time.timeScale = 1f;
		}
	}

	void Start () {
		isDead_BO = false;
	}
	public void Reset()
	{
		
		isDead_BO = true;
		death.SetActive(true);
		death.gameObject.transform.parent = null;
		
		
	}
	public void ResetGame(){
		isDead_BO = false;
		Application.LoadLevel (Application.loadedLevel);//restart the level
		
	}
	public void MainMenu(){
		Application.LoadLevel ("MainMenu");//restart the level
		
	}


	public IEnumerator StartGame()
	{
		Time.timeScale = 1f;
		sleep.SetBool("sleep", true);
		isDead_BO = false;
		yield return new WaitForSeconds(2);
		Application.LoadLevel ("MashEmUp");

	}

	public void ButtonStartGame(){
		StartCoroutine (StartGame());
		Debug.Log("swag");
	}

	public void Credits()
	{
		
		Application.LoadLevel ("Credits");
		
	}
//	public void MainMenu()
//	{
//		
//		Application.LoadLevel ("MainMenu");
//		
//	}

//	public void StartGame()
//	{
//		healthReset.GetComponent<player_controller>().Startgame ();
//		Time.timeScale = 1.0f;
//		Application.LoadLevel ("Tutorial");
//	}



    // Use this for initialization

	// Update is called once per frame
//	void Update () {
//	
//	}
}
