﻿using UnityEngine;
using System.Collections;

[RequireComponent (typeof(AudioSource))]
public class EnemyController : MonoBehaviour {

	private Transform player_GO;

	public bool isDead_BO;

//	private GameObject DeadMenu_Canvas_GO;

	private PlayerController PlayerController;

	public int scoreValue;

	public float speed_ft;

	public GameObject SceneCont;

	int i = 0;

	public GameObject particle;

	public AudioClip playsound;	//The sound
	public AudioClip playsounddead;	//The sound
	public AudioClip playsoundplayer;

	AudioSource audio;	//The Sound Player


	// Use this for initialization
	void Start () {
	
		audio = GetComponent<AudioSource> (); 
		isDead_BO = false;
//
//		DeadMenu_Canvas_GO = GameObject.FindGameObjectWithTag("Canvas");

		SceneCont = GameObject.FindWithTag("SceneCont");

		player_GO = GameObject.FindWithTag("Player").transform;

		PlayerController = player_GO.gameObject.GetComponent<PlayerController>();
	
		speed_ft = SpawnController.speed_ft;

		transform.SetParent(GameObject.FindWithTag("EnemyGroup").transform);

	}
	
	// Update is called once per frame
	void Update () {

//		if (isDead_BO) {
//			DeadMenu_Canvas_GO.SetActive (true);
//			Time.timeScale = 0f;
//			//sword_GO.SetActive(false);
//		} else {
//			DeadMenu_Canvas_GO.SetActive(false);
//			Time.timeScale = 1f;
//		}


		if(transform.position.x <= player_GO.transform.position.x)
		{
			transform.localScale = new Vector3 (0.5f,0.5f,1f);

		} else {
			transform.localScale = new Vector3 (-0.5f,0.5f,1f);
		}
		if(i == 0){
			this.gameObject.transform.position = Vector3.MoveTowards(this.gameObject.transform.position,player_GO.transform.position, speed_ft*Time.deltaTime);
		}
		if(i == 1){
			this.gameObject.transform.position = Vector3.MoveTowards(this.gameObject.transform.position, player_GO.transform.position , -10f * Time.deltaTime);
		}
		if(i == 2){
			this.gameObject.transform.position = Vector3.MoveTowards(this.gameObject.transform.position, player_GO.transform.position , -40f * Time.deltaTime);
		}
		if(i == -1){

		}

	}
	void OnCollisionEnter2D(Collision2D col){
		Debug.Log(col);
		if(col.collider.CompareTag("Sword"))
		{
			audio.PlayOneShot(playsounddead, 1F);
			PlayerController.AddScore (100);
			i = 1;
			GameObject particles = Instantiate(particle ,this.transform.position,Quaternion.identity) as GameObject;
			particles.gameObject.transform.SetParent(this.gameObject.transform);
			//			Destroy(this.gameObject);
		}
		if(col.collider.CompareTag("ChickenBat"))
		{
			audio.PlayOneShot(playsound, 1F);
			PlayerController.AddScore (50);
//			scoreValue = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>().AddScore(50);
			i = 2;
			GameObject particles = Instantiate(particle ,this.transform.position,Quaternion.identity) as GameObject;
			particles.gameObject.transform.SetParent(this.gameObject.transform);

			//			Destroy(this.gameObject);
		}
		if(col.collider.CompareTag("Player"))
		{
			audio.PlayOneShot(playsoundplayer, 1F);
			SceneCont.transform.GetComponent<SceneController>().Reset();
			player_GO.gameObject.SetActive(false);




		}
		
		if(col.collider.CompareTag("Tear"))
		{
			audio.PlayOneShot(playsounddead, 1F);
			PlayerController.AddScore (50);
//			scoreValue = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>().AddScore(50);
			i = -1;
			this.gameObject.GetComponent<Rigidbody2D>().gravityScale = 1f;
//			Instantiate(particle ,col.transform.position,Quaternion.identity);
			Destroy(col.gameObject, 0.1f);
//			Destroy(this.gameObject, 0.1f);
		}
		if(col.collider.CompareTag("Ground"))
		{
			Instantiate(particle ,this.transform.position,Quaternion.identity);
			Destroy(this.gameObject, 0.5f);

		}
		if(col.collider.CompareTag("Wall"))
		{
			//Instantiate(particle ,this.transform.position,Quaternion.identity);
			Destroy(this.gameObject, 0.5f);
			
		}

	}


}
